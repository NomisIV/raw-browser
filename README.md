# `raw-browser`: A **R**adically **A**wesome **W**eb-browser

## Motivation

Web browsers have become big and bloated,
and generally don't conform to the UNIX standard of doing one thing and doing it well.
`raw-browser` aims to solve this by being an ultra minimalistic web browser,
without any GUI chrome.
It achieves this by using the webkit2gtk browser engine,
which often is already installed on the system.
Because of this, the compiled binary has a minimal size of less than 0.1MB![^1]

[^1]: Do note that this file size doesn't include webkit2gtk itself,
or the other runtime dependencies, as they are linked dynamically.

### Why not use [suckless' surf](https://surf.suckless.org/)?

Because it doesn't work on wayland,
and because the nature of being *supposed* to hack around in the source code
isn't particularly well suited to NixOS.

## Installation

Currently the only official release channel is the nix flake in this repo,
and the only supported platform is x86_64-linux.

## Usage

```console
$ raw-browser https://nomisiv.com
$ raw-browser-open
```

## Developing

### Documentation

The rust crate for webkit2gtk is rather poorly documented.
Instead I recommend using the official webkit2gtk documentation,
which can be found [here](https://webkitgtk.org/reference/webkit2gtk/stable/).

The [luakit](https://github.com/luakit/luakit) source code is also a good
source of information.

### Future Features

- History menu: A list of the navigation history stack
- Commands? Kind of like luakit
