config_home=${XDG_CONFIG_HOME:-$HOME/.config}
data_home=${XDG_DATA_HOME:-$HOME/.local/share}

config_file="$config_home/raw-browser/config.json"

history_file=$(jq -r '.["history-file"]' "$config_file" | envsubst)
[ "$history_file" == "null" ] && history_file="$data_home/raw-browser/history"
[ -f "$history_file" ] || (mkdir -p "$(dirname "$history_file")" && touch "$history_file")

search_query=$(jq -r '.["search-query"]' "$config_file")
[ "$search_query" == "null" ] && search_query="https://duckduckgo.com/?q="

# TODO: Most dmenu-alikes assumes that you want to select one of the entries,
# however this might not be the case.
# Perhaps it would be better to use an internal search bar instead...
menu_cmd=$(jq -r '.["menu-command"]' "$config_file")
[ "$menu_cmd" == "null" ] && menu_cmd="dmenu"

url=$(sh -c "sort -u < \"$history_file\" | $menu_cmd")
echo "$url" >> "$history_file"

if echo "$url" | grep -E '^https?://'
then raw-browser "$url"
elif echo "$url" | grep -E '^([A-Za-z0-9]+\.)[A-Za-z0-9]{2,}(/[A-Za-z0-9])*/?'
then raw-browser "https://$url"
else raw-browser "$search_query$(echo "\"$url\"" | jq -r "@uri")" # Url-encode with jq
fi

