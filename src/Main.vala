class RawBrowser : Gtk.Window {
    private const string VERSION = "0.3.0";
    private const string NAME = "raw-browser";
    private static string config_dir = Environment.get_user_config_dir() + "/" + NAME;
    private static string cache_dir = Environment.get_user_cache_dir() + "/" + NAME;
    private static string favicon_dir = cache_dir + "/favicons";
    private static string data_dir = Environment.get_user_data_dir() + "/" + NAME;
    private static string config_path = config_dir + "/config.json";
    private static string style_path = config_dir + "/style.css";
    private static string script_path = config_dir + "/script.js";

    private static bool version = false;

    private const GLib.OptionEntry[] options = {
        {
            "version",
            '\0',
            OptionFlags.NONE,
            OptionArg.NONE,
            ref version,
            "Print version and exit",
            null
        },
        {
            "config",
            'c',
            OptionFlags.NONE,
            OptionArg.FILENAME,
            ref config_path,
            "Path to the configuration file",
            "FILE"
        },
        { null }
    };

    private WebKit.WebContext init_webview_context() {
        // NOTE: I only construct it as a GObject so that I won't have to ship around custom vapi bindings,
        // since the GObject Introspection file the vapi is based on is borked,
        // leading to a protected (and thus uncallable) constructor.
        var manager = GLib.Object.new(typeof(WebKit.WebsiteDataManager),
            "base-cache-directory", cache_dir,
            "base-data-directory", data_dir,
            null
        ) as WebKit.WebsiteDataManager;

        var context = new WebKit.WebContext.with_website_data_manager(manager);

        context.set_favicon_database_directory(favicon_dir);

        return context;
    }

    private WebKit.UserContentManager init_webview_user_content() {
        var manager = new WebKit.UserContentManager();

        // Load user stylesheet (~/.config/raw-browser/style.css)
        var style_file = File.new_for_path(style_path);
        if (style_file.query_exists()) {
            try {
                var dis = new DataInputStream(style_file.read());
                string line, style_text = "";
                while ((line = dis.read_line()) != null) { style_text += line; }
                var user_style_sheet = new WebKit.UserStyleSheet(
                    style_text,
                    WebKit.UserContentInjectedFrames.ALL_FRAMES,
                    WebKit.UserStyleLevel.AUTHOR,
                    null,
                    null
                );
                manager.add_style_sheet(user_style_sheet);
            } catch (Error e) {
                printerr("%s", e.message);
            }
        }

        // TODO: Allow multiple scripts in a script folder?
        // Load user script (~/.config/raw-browser/script.js)
        var script_file = File.new_for_path(script_path);
        if (script_file.query_exists()) {
            try {
                var dis = new DataInputStream(script_file.read());
                string line, script_text = "";
                while ((line = dis.read_line()) != null) { script_text += line; }
                var user_script = new WebKit.UserScript(
                    script_text,
                    WebKit.UserContentInjectedFrames.ALL_FRAMES,
                    WebKit.UserScriptInjectionTime.END,
                    null,
                    null
                );
                manager.add_script(user_script);
            } catch (Error e) {
                printerr("%s", e.message);
            }
        }

        return manager;
    }

    private void init_settings(WebKit.Settings settings, Json.Object config) {
        settings.set_print_backgrounds(true);
        settings.set_default_charset("UTF8");

        // Storage and caching
        settings.set_enable_page_cache(
            config.get_boolean_member_with_default("page-cache", false)
        );

        settings.set_enable_html5_local_storage(
            config.get_boolean_member_with_default("local-storage", false)
        );

        settings.set_enable_developer_extras(
            config.get_boolean_member_with_default("devtools", false)
        );

        // Navigation
        settings.set_enable_back_forward_navigation_gestures(
            config.get_boolean_member_with_default("gestures", true)
        );

        settings.set_enable_smooth_scrolling(
            config.get_boolean_member_with_default("smooth_scrolling", false)
        );

        // Features and APIs
        settings.set_enable_javascript(
            config.get_boolean_member_with_default("javascript", true)
        );

        settings.set_enable_java(
            config.get_boolean_member_with_default("java", false)
        );

        settings.set_hardware_acceleration_policy(
            config.get_boolean_member_with_default("hardware-acceleration", true)
                ? WebKit.HardwareAccelerationPolicy.ALWAYS
                : WebKit.HardwareAccelerationPolicy.NEVER
        );

        settings.set_enable_webgl(
            config.get_boolean_member_with_default("webgl", true)
        );

        settings.set_enable_webaudio(
            config.get_boolean_member_with_default("webaudio", true)
        );

        // Fonts
        var font_config = config.get_object_member("font");
        var font_size_config = font_config.get_object_member("size");

        settings.set_default_font_size(
            (uint32)(font_size_config.get_int_member_with_default("default", 16))
        );

        settings.set_default_monospace_font_size(
            (uint32)(font_size_config.get_int_member_with_default("monospace", 12))
        );

        settings.set_minimum_font_size(
            (uint32)(font_size_config.get_int_member_with_default("minimum", 0))
        );

        if (font_config.has_member("default") && font_config.get_member("default").get_string() != null) {
            settings.set_default_font_family(font_config.get_string_member("default"));
        }

        if (font_config.has_member("cursive") && font_config.get_member("cursive").get_string() != null) {
            settings.set_cursive_font_family(font_config.get_string_member("cursive"));
        }

        if (font_config.has_member("sans-serif") && font_config.get_member("sans-serif").get_string() != null) {
            settings.set_sans_serif_font_family(font_config.get_string_member("sans-serif"));
        }

        if (font_config.has_member("serif") && font_config.get_member("serif").get_string() != null) {
            settings.set_serif_font_family(font_config.get_string_member("serif"));
        }

        if (font_config.has_member("monospace") && font_config.get_member("monospace").get_string() != null) {
            settings.set_monospace_font_family(font_config.get_string_member("monospace"));
        }

        if (font_config.has_member("pictograph") && font_config.get_member("pictograph").get_string() != null) {
            settings.set_pictograph_font_family(font_config.get_string_member("pictograph"));
        }

        // Privacy
        settings.set_enable_dns_prefetching(
            config.get_boolean_member_with_default("dns-prefetching", false)
        );

        settings.set_enable_hyperlink_auditing(
            config.get_boolean_member_with_default("hyperlink-auditing", false)
        );

        // TODO: These settings can potentially pose a security risk when opening untrusted local files. Add an option to enable them
        settings.set_allow_file_access_from_file_urls(true);
        settings.set_allow_universal_access_from_file_urls(true);

        // Media
        settings.set_enable_media(true);
        settings.set_enable_media_capabilities(true);
        settings.set_enable_media_stream(true);
        settings.set_enable_mediasource(true);
        settings.set_enable_mock_capture_devices(true);

        // User agent
        settings.set_user_agent(
            config.get_string_member_with_default("user-agent", NAME + "/" + VERSION)
        );
    }

    private WebKit.WebView init_webview() throws GLib.Error {
        var context = init_webview_context();
        var user_content = init_webview_user_content();

        // NOTE: What the actual fuck??
        // Why should it be so fucking difficult to create a WebView?
        // It already has a constructor, in fact it has multiple, which is more than most classes.
        // But since they all are mutually exclusive I cannot set all the properties I want at once.
        // So as a last resort I am manually constructing it as a GObject,
        // because it's too fucking handicapped to construct itself.
        // Thanks, luakit, for showing me that this insanity is possible in the first place.
        // And thanks Stack Overflow for showing me how I port insanity to Vala.
        var web_view = GLib.Object.new(typeof(WebKit.WebView),
            "web-context", context,
            "user-content-manager", user_content,
            null
        ) as WebKit.WebView;

        var parser = new Json.Parser();
        parser.load_from_file(config_path); // This throws GLib.Error

        var config = new Json.Object();
        if (parser.get_root() != null) {
            config = parser.get_root().get_object();
        }

        var settings = web_view.get_settings();
        init_settings(settings, config);
        web_view.set_settings(settings);

        var background_color = Gdk.RGBA();
        background_color.parse(
            config.get_string_member_with_default("background", "#ffffff")
        );
        web_view.set_background_color(background_color);

        web_view.button_press_event.connect((event_button) => {
            switch (event_button.button) {
                case 8:
                    web_view.go_back();
                    break;
                case 9:
                    web_view.go_forward();
                    break;
            }
            return false;
        });

        web_view.key_press_event.connect((event) => {
            switch(Gdk.keyval_name(event.keyval)) {
                case "F5":
                    web_view.reload();
                    return true;
            }

            if (event.state == Gdk.ModifierType.CONTROL_MASK) {
                switch(Gdk.keyval_name(event.keyval)) {
                    case "u":
                        // Copy the current URL to clipboard
                        var display = this.get_display();
                        var clipboard = Gtk.Clipboard.get_for_display(display, Gdk.SELECTION_CLIPBOARD);
                        var uri = web_view.get_uri();
                        clipboard.set_text(uri, -1);

                        // and show notification
                        var notification = new Notify.Notification("URL copied to clipboard", uri, null);
                        try {
                            notification.show();
                        } catch (Error e) {}
                        return true;

                    case "k":
                        var history_file = config.get_string_member("history-file");
                        var menu_cmd = config.get_string_member("menu-command");
                        var search_query = config.get_string_member("search-query");
                        string stdout;
                        string stderr;
                        int status;
                        try {
                            Process.spawn_command_line_sync(
                                "sh -c \"sort -u < \\\"" +
                                history_file +
                                "\\\" | " +
                                menu_cmd +
                                "\"",
                                out stdout,
                                out stderr,
                                out status
                            );
                            stdout.chomp();
                            string url;
                            if (/^https?:\/\//.match(stdout)) {
                                url = stdout;
                            } else if (/^([A-Za-z0-9]+\.)[A-Za-z0-9]{2,}(\/[A-Za-z0-9])*\/?/.match(stdout)) {
                                url = "https://" + stdout;
                            } else {
                                url = search_query + stdout;
                            }
                            web_view.load_uri(url);
                        } catch (SpawnError e) {
                            // TODO: Do something here
                        }
                        return true;
                }
            }

            return false;
        });

        web_view.delete_event.connect((event) => {
            web_view.terminate_web_process();
            return false;
        });

        web_view.destroy.connect(Gtk.main_quit);

        return web_view;
    }

    private void update_favicon(WebKit.WebView web_view, Gtk.Image favicon) {
        var fdb = web_view.get_context().get_favicon_database();
        fdb.get_favicon.begin(web_view.get_uri(), null, (object, result) => {
            try {
                var surface = (Cairo.ImageSurface)(fdb.get_favicon.end(result)); // This throws GLib.Error
                var w = surface.get_width();
                var h = surface.get_height();
                var size = 16; // TODO: Make this into an option
                var pixbuf = Gdk.pixbuf_get_from_surface(surface, 0, 0, w, h)
                    .scale_simple(size, size, Gdk.InterpType.BILINEAR);

                favicon.set_visible(true);
                favicon.set_from_pixbuf(pixbuf);
            } catch (Error e) {
                favicon.set_visible(false);
            }
        });
    }
    
    public void load_url(string path) {
        var file = File.new_for_commandline_arg(path);
        try {
            var margin = 4; // TODO: Make this into an option

            var url = new Gtk.Label(null);
            url.set_margin_top(margin);
            url.set_margin_bottom(margin);
            url.set_margin_start(margin);

            var title = new Gtk.Label(null);
            title.set_margin_top(margin);
            title.set_margin_bottom(margin);
            title.set_margin_start(margin);

            var web_view = init_webview();
            var progress_bar = new Gtk.ProgressBar();

            var favicon = new Gtk.Image();
            favicon.set_margin_top(margin);
            favicon.set_margin_bottom(margin);
            favicon.set_margin_start(margin);

            web_view
                .get_context()
                .get_favicon_database()
                .favicon_changed
                .connect((page_uri, favicon_uri) => {
                    if (page_uri == web_view.get_uri()) {
                        update_favicon(web_view, favicon);
                    }
                });

            web_view.decide_policy.connect((decision, type) => {
                if (type == WebKit.PolicyDecisionType.NAVIGATION_ACTION) {
                    var np = decision as WebKit.NavigationPolicyDecision;
                    var na = np.get_navigation_action();
                    var uri = na.get_request();
                    this.set_title("raw-browser " + uri.get_uri());
                    url.set_label("(" + uri.get_uri() + ")");
                }

                return false;
            });

            web_view.load_changed.connect((load_event) => {
                switch (load_event) {
                    case WebKit.LoadEvent.STARTED:
                        progress_bar.set_visible(true);
                        break;
                    case WebKit.LoadEvent.FINISHED:
                        progress_bar.set_visible(false);
                        update_favicon(web_view, favicon);
                        title.set_label(web_view.get_title());
                        break;
                    default:
                        break;
                }
            });

            web_view.notify.connect((sender, property) => {
                if (property.name == "estimated-load-progress") {
                    var progress = web_view.get_estimated_load_progress();
                    progress_bar.set_fraction(progress);
                }
            });

            web_view.set_name("web_view");

            var file_uri = file.get_uri_scheme();

            if (file_uri != "https" && file_uri != "http" && file_uri != "file") {
                printerr("Unsupported URI: %s\n", file_uri);
                // TODO: Exit with error code
                return;
            }

            web_view.load_uri(file.get_uri());

            var status_bar = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 0);
            status_bar.pack_start(favicon, false, false, 0);
            status_bar.pack_start(title, false, false, 1);
            status_bar.pack_start(url, false, false, 2);

            var box = new Gtk.Box(Gtk.Orientation.VERTICAL, 0);
            box.set_homogeneous(false);
            box.pack_start(web_view, true, true, 0);
            box.pack_start(progress_bar, false, false, 1);
            box.pack_end(status_bar, false, false);

            this.set_title("raw-browser " + file.get_uri());
            this.set_default_size(800, 600);
            this.add(box);
            this.show_all();
        } catch (Error e) {
            printerr("%s\n", e.message);
            // TODO: Exit with error code
        }
    }

    // TODO: open tab in new windows doesn't work
    public static int main(string[] args) {
        var option_context = new OptionContext("URL");
        option_context.set_help_enabled(true);
        option_context.add_main_entries(RawBrowser.options, null);

        try {
            option_context.parse(ref args);
        } catch (OptionError e) {
            printerr("Could not parse command line entry: %s\n", e.message);
            return 1;
        }

        // Handle --version
        if (version) {
            print("%s version %s\n", NAME, VERSION);
            return 0;
        }

        if (args[1] == null) {
            // TODO: Open blank page instead
            printerr("No URL to open. Exiting.\n");
            return 1;
        } 

        Notify.init("raw-browser");

        Gtk.init(ref args);

        var raw_browser = new RawBrowser();

        raw_browser.load_url(args[1]);

        Gtk.main();

        return 0;
    }
}

