{
  description = "An ultra minimalistic browser based on webkit2gtk";

  outputs = {
    self,
    nixpkgs,
  }: let
    systems = ["x86_64-linux" "aarch64-linux"];

    config = system: let
      pkgs = nixpkgs.legacyPackages.${system};

      deps = with pkgs; [
        vala
        pkg-config
        webkitgtk_4_1
        gtk3
        glib-networking
        gst_all_1.gstreamer
        gst_all_1.gst-vaapi
        gst_all_1.gst-plugins-bad
        gst_all_1.gst-plugins-ugly
        gst_all_1.gst-plugins-good
        gst_all_1.gst-plugins-base
        json-glib
        libnotify
      ];
    in {
      packages.${system} = rec {
        default = raw-browser;

        raw-browser = pkgs.stdenv.mkDerivation rec {
          pname = "raw-browser";
          version = "0.3.0";

          src = builtins.path {
            path = ./.;
            name = pname;
          };

          nativeBuildInputs = with pkgs; [
            meson
            ninja
            wrapGAppsHook
          ];

          buildInputs = deps;
        };

        raw-browser-open = pkgs.writeShellApplication {
          name = "raw-browser-open";
          runtimeInputs = with pkgs; [raw-browser jq gettext];
          text = builtins.readFile ./raw-browser-open.sh;
        };
      };

      homeManagerModules.default = {
        config,
        lib,
        pkgs,
        ...
      }:
        with lib; let
          cfg = config.programs.raw-browser;
        in {
          options.programs.raw-browser = {
            enable = mkEnableOption "Enable raw-browser";
            default = mkEnableOption "Make raw-browser the default browser";

            config = mkOption {
              type = types.nullOr types.attrs;
              default = null;
              # TODO: Turn this into a submodule with all options declared
            };

            style = mkOption {
              type = types.nullOr types.str;
              default = null;
            };

            script = mkOption {
              type = types.nullOr types.str;
              default = null;
            };
          };

          config = mkIf cfg.enable {
            home = {
              packages = with pkgs; [raw-browser];
              sessionVariables = mkIf cfg.default {BROWSER = "raw-browser";};
            };

            xdg.configFile = {
              "raw-browser/config.json".text = builtins.toJSON cfg.config;

              "raw-browser/style.css" = mkIf (cfg.style != null) {
                text = cfg.style;
              };

              "raw-browser/script.js" = mkIf (cfg.script != null) {
                text = cfg.script;
              };
            };
          };
        };

      devShells.${system}.default = pkgs.mkShell {
        buildInputs =
          deps
          ++ (with pkgs; [
            vala-language-server
            nil
            nodePackages.bash-language-server
            meson
            ninja
          ]);
        GIO_MODULE_DIR = "${pkgs.glib-networking}/lib/gio/modules";
      };

      formatter.${system} = pkgs.alejandra;
    };
    mergeSystems = acc: system:
      with nixpkgs.legacyPackages.${system};
        lib.attrsets.recursiveUpdate acc (config system);
  in
    builtins.foldl' mergeSystems {} systems;
}
